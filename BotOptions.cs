﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RoleWithIt
{
    public class BotOptions
    {
        public string Token { get; set; }
        public string Invite { get; set; }
        public string SqliteDb { get; set; }
    }
}
