﻿using Discord;
using Discord.Commands;
using Discord.WebSocket;
using Microsoft.Extensions.DependencyInjection;
using RoleWithIt.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace RoleWithIt
{
    public class CommandHandlingService
    {
        private readonly CommandService _commands;
        private readonly DiscordSocketClient _discord;
        private readonly IServiceProvider _services;
        public IRoleData _roledata { get; set; }

        public CommandHandlingService(IServiceProvider services)
        {
            _commands = services.GetRequiredService<CommandService>();
            _discord = services.GetRequiredService<DiscordSocketClient>();
            _roledata = services.GetRequiredService<IRoleData>();
            _services = services;

            _commands.CommandExecuted += CommandExecutedAsync;
            _discord.MessageReceived += MessageReceivedAsync;
        }

        public async Task InitialiseAsync()
        {
            _commands.AddTypeReader<IUser>(new UserTypeReader());
            await _commands.AddModulesAsync(Assembly.GetEntryAssembly(), _services);
        }

        public async Task MessageReceivedAsync(SocketMessage rawMessage)
        {
            if (!(rawMessage is SocketUserMessage message)) return;
            if (message.Source != Discord.MessageSource.User) return;
            bool process = false;

            var argPos = 0;
            if (message.Channel is IGuildChannel guildChannel)
            {
                var prefix = await _roledata.GetGuildCommandPrefix(guildChannel.GuildId);
                if (message.HasStringPrefix(prefix, ref argPos)) process = true;
            }

            if (message.HasMentionPrefix(_discord.CurrentUser, ref argPos)) process = true;

            if (process)
            {
                var context = new SocketCommandContext(_discord, message);
                await _commands.ExecuteAsync(context, argPos, _services);
            }
        }

        public async Task CommandExecutedAsync(Optional<CommandInfo> command, ICommandContext context, IResult result)
        {
            if (!command.IsSpecified)
                return;

            if (result.IsSuccess)
                return;

            await context.Channel.SendMessageAsync($"error: {result}");
        }

        public async Task ReactionAddedAsync(Cacheable<IUserMessage, ulong> cachedMessage,
                                                ISocketMessageChannel originChannel, SocketReaction reaction)
        {
            var message = await cachedMessage.GetOrDownloadAsync();
            if (reaction.UserId == _discord.CurrentUser.Id) return;
            if (message.Author.Id == _discord.CurrentUser.Id)
            {
            }
        }
    }
}
