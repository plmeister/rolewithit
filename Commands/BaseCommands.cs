﻿using Discord;
using Discord.Commands;
using RoleWithIt.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoleWithIt.Commands
{
    public class BaseCommands : ModuleBase<SocketCommandContext>
    {

        public IRoleData _roledata { get; set; }
        public CommandService _commands { get; set; }

        [Command("ping")]
        [Alias("pong", "hello")]
        [Summary("Check if the bot is running")]
        public Task PingAsync() => ReplyAsync("pong!");

        [Command("prefix")]
        [RequireContext(ContextType.Guild)]
        [Summary("Gets or sets the command prefix used by this bot")]
        public Task Prefix([Summary("If set, then use this prefix in future - if not, then just display the current prefix")] string prefix = "")
        {
            if (prefix == "")
            {
                return ReplyAsync($"Current command prefix is {_roledata.GetGuildCommandPrefix(Context.Guild.Id)}");
            }
            else
            {
                _roledata.SetGuildCommandPrefix(Context.Guild.Id, prefix);
                return ReplyAsync($"Command prefix set to {_roledata.GetGuildCommandPrefix(Context.Guild.Id)}");
            }
        }

        [Command("help")]
        public Task Help(params string[] args)
        {
            if (args.Length == 0)
            {
                var sb = new StringBuilder();
                foreach (var x in _commands.Commands.OrderBy(c => c.Module.Name).ThenBy(c => c.Name))
                {
                    sb.AppendLine($"{(x.Module.Group == null ? x.Name : x.Module.Group + " " + x.Name)}");
                }
                return ReplyAsync(embed: new EmbedBuilder()
                    .WithTitle("Bot commands")
                    .WithColor(Color.Blue)
                    .WithDescription(sb.ToString())
                    .Build());
            }

            var result = _commands.Search(string.Join(" ", args));
            if (result.IsSuccess)
            {
                var found = result.Commands[0].Command;
                var sb = new StringBuilder();
                if (found.Module.Group != "")
                    sb.AppendLine($"Group: ` {found.Module.Group} `");
                
                
                if (found.Aliases.Count > 1)
                    sb.AppendLine($"Aliases: ` {string.Join(", ", found.Aliases)} `");
                
                if (found.Parameters.Count > 0)
                {
                    sb.AppendLine("Parameters:");
                    sb.AppendLine("```");
                    foreach (var p in found.Parameters)
                    {
                        var display = p.Name;
                        if (p.IsOptional) display = $"[{display} = {p.DefaultValue}]";
                        if (p.IsMultiple) display = display + "*";
                        sb.AppendLine($"{display} - {p.Summary}");
                    }
                    sb.AppendLine("```");
                }

                if (found.Summary != "" && found.Summary != null)
                {
                    sb.AppendLine($"Summary: ``` {found.Summary} ```");
                }

                var embed = new EmbedBuilder()
                    .WithTitle((found.Module.Group == null ? found.Name : found.Module.Group + " " + found.Name))
                    .WithColor(Color.Blue)
                    .WithDescription(sb.ToString())
                    .Build();
                
                return ReplyAsync(embed: embed);
            }
            else
            {

                return ReplyAsync($"Couldn't find command");
            }
        }
    }
}
