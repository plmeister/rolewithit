﻿using Discord;
using Discord.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoleWithIt.Commands
{
    [Group("debug")]
    public class DebugModule : ModuleBase<SocketCommandContext>
    {
        [Command("cache")]
        [RequireContext(ContextType.Guild)]
        public async Task CachedUsers()
        {
            await ReplyAsync($"{Context.Guild.DownloadedMemberCount} / {Context.Guild.MemberCount} users cached");
        }
        [Command("checkuser")]
        [RequireContext(ContextType.Guild)]
        public async Task CheckUsers(IUser user)
        {
            if (user is IGuildUser g)
            {
                await ReplyAsync($"Username: '{g.Username}', Nickname: '{g.Nickname ?? ""}'");
            }
        }
    }
}
