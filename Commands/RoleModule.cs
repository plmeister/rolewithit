﻿using Discord.Commands;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;
using Discord;
using RoleWithIt.Data;
using Discord.WebSocket;
using RoleWithIt.Services;

namespace RoleWithIt.Commands
{
    [Group("role")]
    public class RoleModule : ModuleBase<SocketCommandContext>
    {
        public IRoleData _roledata { get; set; }

        public RoleService _roleservice { get; set; }
        [Summary("This command adds a list of users into a role")]
        [Command("add")]
        [RequireContext(ContextType.Guild)]
        [RequireBotPermission(GuildPermission.ManageRoles)]
        public async Task AddRoleAsync([Summary("The role to which to add users")] IRole role, [Summary("A list of users to add")] params IUser[] users)
        {
            var usersAdded = await _roleservice.addUsersToRole(Context.User as IGuildUser, Context.Guild.GetRole(role.Id), users.Select(u => Context.Guild.GetUser(u.Id)));

            await ReplyAsync(embed: new EmbedBuilder()
                .WithColor(Color.Blue)
                .WithDescription(@$"{usersAdded.Count()} users added to @{role.Name}: {MarkdownList(usersAdded, u => u.Username)}")
                .Build());
        }

        [Command("del")]
        [Summary("This command removes a list of users from a role")]
        [RequireContext(ContextType.Guild)]
        [RequireBotPermission(GuildPermission.ManageRoles)]
        public async Task DelRoleAsync([Summary("The role from which to remove users")] IRole role, [Summary("A list of users to remove")] params IUser[] users)
        {
            var removed = await _roleservice.delUsersFromRole(
                Context.User as IGuildUser,
                Context.Guild.GetRole(role.Id),
                users.Select(u => Context.Guild.GetUser(u.Id))
                );

            await ReplyAsync(embed: new EmbedBuilder()
                .WithColor(Color.Blue)
                .WithDescription(@$"{removed.Count()} users removed from @{role.Name}: {MarkdownList(removed, u => u.Username)}")
                .Build());
        }

        [Command("clear")]
        [Summary("Removes all users from the specified role")]
        [RequireContext(ContextType.Guild)]
        [RequireBotPermission(GuildPermission.ManageRoles)]
        public async Task ClearRoleAsync(IRole role)
        {
            var users = await _roleservice.removeAllUsersFromRole(Context.User as IGuildUser, Context.Guild.GetRole(role.Id));
            await ReplyAsync(embed: new EmbedBuilder()
                .WithColor(Color.Blue)
                .WithDescription(@$"Removed the following {users.Count} users from @{role.Name}: {MarkdownList(users, u => u.Username)}")
                .Build());
        }


        [Command("permit")]
        [Summary("Allows users in a certain role to use the bot to add/remove other users to/from a specific role")]
        [RequireContext(ContextType.Guild)]
        [RequireUserPermission(GuildPermission.ManageRoles)]
        public async Task PermitUser([Summary("The role to be managed")] IRole childRole, [Summary("The role which should be allowed to manage the other role")] IRole parentRole)
        {
            await _roleservice.permitRole(Context.Guild.Id, childRole.Id, parentRole.Id);
            await ReplyAsync(embed: new EmbedBuilder()
                .WithColor(Color.Blue)
                .WithDescription($"`@{parentRole.Name}` is allowed to manage role `@{childRole.Name}`")
                .Build());
        }

        [Command("deny")]
        [Summary("Prevents users in a certain role to use the bot to add/remove other users to/from a specific role")]
        [RequireContext(ContextType.Guild)]
        [RequireUserPermission(GuildPermission.ManageRoles)]
        public async Task DenyUser([Summary("The role to be managed")] IRole childRole, [Summary("The role which should not be allowed to manage the other role")] IRole parentRole)
        {
            await _roleservice.denyRole(Context.Guild.Id, childRole.Id, parentRole.Id);
            await ReplyAsync(embed: new EmbedBuilder()
                .WithColor(Color.Blue)
                .WithDescription($"`@{parentRole.Name}` is not allowed to manage role `@{childRole.Name}`")
                .Build());
        }

        [Command("listperms")]
        [Summary("List which roles are permitted to manage which other roles")]
        [RequireContext(ContextType.Guild)]
        public async Task ListPermissions()
        {
            var perms = await _roledata.ListRolePermissions(Context.Guild.Id);
            var sb = new StringBuilder();
            Func<ulong, string> roleName = (id) => Context.Guild.GetRole(id).Name;
            foreach (var p in perms)
            {
                sb.AppendLine($"`@{roleName(p.ParentRoleId)}` can manage: ```");
                foreach (var child in p.ChildRoleIds)
                {
                    sb.AppendLine(roleName(child));
                }
                sb.AppendLine("```");
            }
            await ReplyAsync(embed: new EmbedBuilder()
                .WithTitle("Current role permissions")
                .WithColor(Color.Blue)
                .WithDescription(sb.ToString())
                .Build());
        }

        [Command("list")]
        [Summary("List the current members of a role")]
        [RequireContext(ContextType.Guild)]
        public Task ListRoleMembers(IRole role)
        {
            var guildRole = Context.Guild.GetRole(role.Id);
            return ReplyAsync(embed: new EmbedBuilder()
                .WithColor(Color.Blue)
                .WithDescription(@$"Role {@role.Name} contains {guildRole.Members.Count()} users: {MarkdownList(guildRole.Members, x => x.Username)}")
                .Build());
        }

        [Command("switch")]
        [Summary("Switch the members of 2 roles")]
        [RequireContext(ContextType.Guild)]
        public async Task SwitchRoles(IRole role1, IRole role2)
        {
            var users1 = await _roleservice.removeAllUsersFromRole(Context.User as IGuildUser, Context.Guild.GetRole(role1.Id));
            var users2 = await _roleservice.removeAllUsersFromRole(Context.User as IGuildUser, Context.Guild.GetRole(role2.Id));

            await _roleservice.addUsersToRole(Context.User as IGuildUser, Context.Guild.GetRole(role2.Id), users1);
            await _roleservice.addUsersToRole(Context.User as IGuildUser, Context.Guild.GetRole(role1.Id), users2);

            var sb = new StringBuilder();
            sb.AppendLine(@$"Role {@role1.Name} contains {Context.Guild.GetRole(role1.Id).Members.Count()} users: {MarkdownList(Context.Guild.GetRole(role1.Id).Members, x => x.Username)}");
            sb.AppendLine(@$"Role {@role2.Name} contains {Context.Guild.GetRole(role2.Id).Members.Count()} users: {MarkdownList(Context.Guild.GetRole(role2.Id).Members, x => x.Username)}");
            await ReplyAsync(embed: new EmbedBuilder()
                .WithColor(Color.Blue)
                .WithDescription(sb.ToString())
                .Build());
        }

        [Command("event")]
        [RequireContext(ContextType.Guild)]
        public async Task EventRole(IRole role)
        {
            var eventRoleResult = await _roleservice.setEventRole(Context.User as IGuildUser, Context.Guild.GetRole(role.Id), Context.Channel);
            if (string.IsNullOrEmpty(eventRoleResult.ErrorMessage))
            {

                await ReplyAsync(embed: new EmbedBuilder()
                    .WithColor(Color.Blue)
                    .WithDescription(@$"
Event: {eventRoleResult.EventText}
Removed {eventRoleResult.UsersRemoved.Count} users:
{MarkdownList(eventRoleResult.UsersRemoved, x => x.Username)}
Added {eventRoleResult.UsersAdded} users:
{MarkdownList(eventRoleResult.UsersAdded, x => x.Username)}
@{role.Name} now contains {eventRoleResult.RoleContains.Count} users:
{MarkdownList(eventRoleResult.RoleContains, x => x.Username)}
")
                    .Build());
            }
            else
            {
                await ReplyAsync(embed: new EmbedBuilder()
                    .WithColor(Color.Blue)
                    .WithDescription(eventRoleResult.ErrorMessage)
                    .Build());
            }
        }

        private string MarkdownList<T>(IEnumerable<T> list, Func<T, string> propertyAccessor, string joinString = "\n", string emptyList = "none")
        {
            if (list == null || list.Count() == 0)
            {
                return $"```\n{emptyList}\n```";
            }
            else
            {
                return $"```\n{string.Join(joinString, list.Select(x => propertyAccessor(x)))}\n```";
            }
        }
    }
}
