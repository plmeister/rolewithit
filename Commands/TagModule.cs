﻿using Discord;
using Discord.Commands;
using RoleWithIt.Data;
using RoleWithIt.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoleWithIt.Commands
{
    [Group("tag")]
    public class TagModule : ModuleBase<SocketCommandContext>
    {
        public IRoleData _roledata { get; set; }
        public TagService _tags { get; set; }

        [Command("set")]
        [Summary("Add a tag to a user's nickname")]
        [RequireContext(ContextType.Guild)]
        public async Task SetTagAsync(string tag, params IUser[] users)
        {
            if (!await _roledata.IsUserAllowedToTag(Context.User as IGuildUser))
            {
                await ReplyAsync($"User '{Context.User.Username}' does not have permission to tag in this server");
                return;
            }
            var tagged = await _tags.tagUsers(tag, users.Cast<IGuildUser>());
            var sb = new StringBuilder();
            foreach (var x in tagged.Values)
            {
                sb.AppendLine(x);
            }
            await ReplyAsync(embed: new EmbedBuilder()
                .WithTitle("Set Tags:")
                .WithColor(Color.Blue)
                .WithDescription(sb.ToString())
                .Build());
        }

        [Command("clear")]
        [Summary("Clear a tag from a user's nickname")]
        [RequireContext(ContextType.Guild)]
        public async Task ClearTagAsync(params IUser[] users)
        {
            if (!await _roledata.IsUserAllowedToTag(Context.User as IGuildUser))
            {
                await ReplyAsync($"User '{Context.User.Username}' does not have permission to tag in this server");
                return;
            }
            var tagged = await _tags.untagUsers(users.Cast<IGuildUser>());

            var sb = new StringBuilder();
            foreach (var x in tagged.Values)
            {
                sb.AppendLine(x);
            }
            await ReplyAsync(embed: new EmbedBuilder()
                .WithTitle("Unset Tags:")
                .WithColor(Color.Blue)
                .WithDescription(sb.ToString())
                .Build());

        }

        [Command("permit")]
        [Summary("Permit a role to modify tags")]
        [RequireContext(ContextType.Guild)]
        public async Task PermitTagRole(IRole role)
        {
            if (!(Context.User as IGuildUser).GuildPermissions.Administrator)
            {
                await ReplyAsync($"{Context.User.Username} is not an admin on this server");
                return;
            }
            await _tags.PermitRole(Context.Guild.Id, role.Id);
            await ReplyAsync(embed: new EmbedBuilder()
                .WithTitle("Permit tag role:")
                .WithColor(Color.Blue)
                .WithDescription($"{role.Name} is permitted to modify tags")
                .Build());
        }

        [Command("deny")]
        [Summary("Deny a role to modify tags")]
        [RequireContext(ContextType.Guild)]
        public async Task DenyTagRole(IRole role)
        {
            if (!(Context.User as IGuildUser).GuildPermissions.Administrator)
            {
                await ReplyAsync($"{Context.User.Username} is not an admin on this server");
                return;
            }
            await _tags.DenyRole(Context.Guild.Id, role.Id);
            await ReplyAsync(embed: new EmbedBuilder()
                .WithTitle("Deny tag role:")
                .WithColor(Color.Blue)
                .WithDescription($"{role.Name} is not permitted to modify tags")
                .Build());
        }

        [Command("listperms")]
        [Summary("Show current list of roles permitted to modify tags")]
        [RequireContext(ContextType.Guild)]
        public async Task ListPerms()
        {
            var perms = await _tags.ListPermissions(Context.Guild.Id);
            var sb = new StringBuilder();
            foreach (var x in perms)
            {
                sb.AppendLine($"@{Context.Guild.Roles.SingleOrDefault(r => r.Id == x.RoleID).Name}");
            }

            if (perms.Count() == 0)
            {
                sb.AppendLine("none");
            }

            await ReplyAsync(embed: new EmbedBuilder()
                .WithTitle("Roles:")
                .WithColor(Color.Blue)
                .WithDescription(sb.ToString())
                .Build());
        }
    }
}
