﻿using System.Collections.Generic;

namespace RoleWithIt.Data
{
    public class AllowedRole
    {
        public ulong ParentRoleId { get; set; }
        public List<ulong> ChildRoleIds { get; set; } = new List<ulong>();
    }
}
