﻿using Discord;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Data;
using Dapper;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.Extensions.Configuration;
using System.Data.SQLite;
using System.IO;
using Microsoft.Extensions.DependencyInjection;

namespace RoleWithIt.Data
{
    public class DapperRoleData : IRoleData
    {
        public const int DBVERSION = 1;

        public class AllowedRoleItem
        {
            public ulong GuildID { get; set; }
            public ulong ParentRoleID { get; set; }
            public ulong ChildRoleID { get; set; }
        }

        IServiceProvider _services;
        public DapperRoleData(IServiceProvider services)
        {
            _services = services;
        }
        public async Task<IDbConnection> GetConnection()
        {
            var botoptions = new BotOptions();
            _services.GetRequiredService<IConfigurationRoot>().GetSection("bot").Bind(botoptions);
            var createTables = false;
            if (!File.Exists(botoptions.SqliteDb))
            {
                SQLiteConnection.CreateFile(botoptions.SqliteDb);
                createTables = true;
            }
            var db = new SQLiteConnection($"Data Source={botoptions.SqliteDb};Version=3;");
            if (createTables)
            {
                await initDatabase(db);
            }

            await upgradeDatabase(db);

            return db;
        }
        public async Task DenyRole(ulong guildId, ulong childRoleId, ulong parentRoleId)
        {
            var sql = "DELETE FROM AllowedRole WHERE GuildID = @GuildID AND ParentRoleID = @ParentRoleID AND ChildRoleID = @ChildRoleID";
            using (var db = await GetConnection())
            {
                await db.ExecuteAsync(sql, new { GuildID = guildId, ParentRoleID = parentRoleId, ChildRoleID = childRoleId });
            }
        }

        public async Task<bool> IsUserAllowedToModifyRole(IGuildUser user, ulong roleId)
        {
            if (user.GuildPermissions.ManageRoles) return true;
            var sql = "SELECT COUNT(1) FROM AllowedRole WHERE GuildID = @GuildID AND ParentRoleID IN @ParentRoleIDS AND ChildRoleID = @ChildRoleID";
            using (var db = await GetConnection())
            {
                var result = await db.ExecuteScalarAsync<int>(sql, new { GuildID = user.GuildId, ParentRoleIDS = user.RoleIds, ChildRoleID = roleId });
                return result > 0;
            }
        }

        public async Task<ReadOnlyCollection<AllowedRole>> ListRolePermissions(ulong guildId)
        {
            var sql = @"SELECT ParentRoleID, ChildRoleID
                        FROM AllowedRole
                        WHERE GuildID = @GuildID";
            using (var db = await GetConnection())
            {
                var perms = await db.QueryAsync<AllowedRoleItem>(sql, new { GuildID = guildId });
                var result = new List<AllowedRole>();
                foreach (var p in perms)
                {
                    var r = result.FirstOrDefault(x => x.ParentRoleId == p.ParentRoleID);
                    if (r == null)
                    {
                        r = new AllowedRole() { ParentRoleId = p.ParentRoleID };
                        result.Add(r);
                    }
                    r.ChildRoleIds.Add(p.ChildRoleID);
                }
                return result.AsReadOnly();
            }

        }

        public async Task<ReadOnlyCollection<TagPermission>> ListTagPermissions(ulong guildId)
        {
            var sql = @"SELECT GuildId, RoleId
                        FROM TagPermissions
                        WHERE GuildID = @GuildID";
            using (var db = await GetConnection())
            {
                var perms = await db.QueryAsync<TagPermission>(sql, new { GuildID = guildId });
                return perms.ToList().AsReadOnly();
            }
        }

        public async Task PermitRole(ulong guildId, ulong childRoleId, ulong parentRoleId)
        {
            var sql = "INSERT OR IGNORE INTO AllowedRole (GuildID, ParentRoleID, ChildRoleID) Values (@GuildID, @ParentRoleID, @ChildRoleID)";
            using (var db = await GetConnection())
            {
                await db.ExecuteAsync(sql, new { GuildID = guildId, ParentRoleID = parentRoleId, ChildRoleID = childRoleId });
            }
        }

        public async Task initDatabase(IDbConnection db)
        {
            await db.ExecuteAsync(
                    @"CREATE TABLE GuildConfig
                      (
                         GuildID                            integer not null,
                         CommandPrefix                      varchar(100) not null
                      )");
            await db.ExecuteAsync(
                    @"CREATE TABLE AllowedRole
                      (
                         GuildID                            integer not null,
                         ParentRoleID                       integer not null,
                         ChildRoleID                        integer not null,
                         UNIQUE(GuildId, ParentRoleID, ChildRoleID)
                      )");
        }
        public async Task upgradeDatabase(IDbConnection db)
        {
            db.Execute(@"CREATE TABLE IF NOT EXISTS Version (VersionNumber  integer not null)");
            var currentVersion = await db.ExecuteScalarAsync<int>(@"SELECT VersionNumber FROM Version");
            if (currentVersion < DBVERSION)
            {
                if (currentVersion == 0)
                {
                    await db.ExecuteAsync(@"INSERT INTO Version (VersionNumber) VALUES (0)");
                }
                for (int ver = currentVersion + 1; ver <= DBVERSION; ver++)
                {
                    await doUpgrade(db, ver);
                }
            }
        }

        public async Task doUpgrade(IDbConnection db, int toVersion)
        {
            switch(toVersion)
            {
                case 1:
                    await db.ExecuteAsync(@"CREATE TABLE TagPermissions (
                                        GuildID  integer not null,
                                        RoleID   integer not null,
                                        UNIQUE(GuildID, RoleID)
                    )");
                    break;
            }

            await db.ExecuteAsync(@"UPDATE Version SET VersionNumber = @NewVersion", new { NewVersion = toVersion });
        }

        public async Task<string> GetGuildCommandPrefix(ulong guildId)
        {
            var sql = "SELECT CommandPrefix FROM GuildConfig WHERE GuildID = @GuildID";
            using (var db = await GetConnection())
            {
                return (await db.QueryAsync<string>(sql, new { GuildID = guildId })).FirstOrDefault() ?? "/";
            }
        }

        public async Task SetGuildCommandPrefix(ulong guildId, string prefix)
        {
            var sql = "SELECT CommandPrefix FROM GuildConfig WHERE GuildID = @GuildID";
            using (var db = await GetConnection())
            {
                var exists = await db.QuerySingleAsync<int>("SELECT COUNT(1) FROM GuildConfig WHERE GuildID = @GuildID", new { GuildID = guildId });
                if (exists == 0)
                {
                    await db.ExecuteAsync("INSERT INTO GuildConfig (GuildID, CommandPrefix) VALUES (@GuildID, @CommandPrefix)", new { GuildID = guildId, CommandPrefix = prefix });
                }
                else
                {
                    await db.ExecuteAsync(@"UPDATE GuildConfig
                                SET CommandPrefix = @CommandPrefix
                                WHERE GuildID = @GuildID", new { GuildID = guildId, CommandPrefix = prefix });
                }
            }
        }

        public async Task PermitTag(ulong guildId, ulong roleId)
        {
            using (var db = await GetConnection())
            {
                await db.ExecuteAsync("INSERT OR IGNORE INTO TagPermissions (GuildID, RoleID) VALUES (@GuildID, @RoleID)", new { GuildID = guildId, RoleID = roleId });
            }
        }

        public async Task DenyTag(ulong guildId, ulong roleId)
        {
            using (var db = await GetConnection())
            {
                await db.ExecuteAsync("DELETE FROM TagPermissions WHERE GuildID = @GuildID AND RoleID = @RoleID", new { GuildID = guildId, RoleID = roleId });
            }
        }

        public async Task<bool> IsUserAllowedToTag(IGuildUser user)
        {
            if (user.GuildPermissions.ManageNicknames) return true;

            using (var db = await GetConnection())
            {
                var perms = await ListTagPermissions(user.GuildId);
                var allowed = perms.Select(p => p.RoleID).Intersect(user.RoleIds).Any();
                return allowed;
            }
        }
    }
}
