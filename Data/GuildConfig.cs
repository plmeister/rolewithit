﻿using System.Collections.Generic;

namespace RoleWithIt.Data
{
    public class GuildConfig
    {
        public ulong GuildId { get; set; }
        public string CommandPrefix { get; set; }
        public List<AllowedRole> RolePermissions { get; set; } = new List<AllowedRole>();
    }
}
