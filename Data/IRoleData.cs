﻿using Discord;
using System.Collections.ObjectModel;
using System.Threading.Tasks;

namespace RoleWithIt.Data
{
    public interface IRoleData
    {
        Task<bool> IsUserAllowedToModifyRole(IGuildUser user, ulong roleId);
        Task<bool> IsUserAllowedToTag(IGuildUser user);
        Task PermitRole(ulong guildId, ulong childRoleId, ulong parentRoleId);
        Task DenyRole(ulong guildId, ulong childRoleId, ulong parentRoleId);
        Task PermitTag(ulong guildId, ulong roleId);
        Task DenyTag(ulong guildId, ulong roleId);
        Task<ReadOnlyCollection<AllowedRole>> ListRolePermissions(ulong guildId);
        Task<ReadOnlyCollection<TagPermission>> ListTagPermissions(ulong guildId);
        Task<string> GetGuildCommandPrefix(ulong guildId);
        Task SetGuildCommandPrefix(ulong guildId, string prefix);
    }
}
