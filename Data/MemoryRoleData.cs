﻿using Discord;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace RoleWithIt.Data
{

    class MemoryRoleData //: IRoleData
    {
        Dictionary<ulong, GuildConfig> data = new Dictionary<ulong, GuildConfig>();
        public MemoryRoleData()
        {
        }

        public bool IsUserAllowedToModifyRole(IGuildUser user, ulong roleId)
        {
            if (user.GuildPermissions.ManageRoles) return true;

            GuildConfig g;
            if (data.TryGetValue(user.GuildId, out g))
            {
                var permittedRoleIds = g.RolePermissions.Where(p => p.ChildRoleIds.Contains(roleId)); 
                return permittedRoleIds.Any(p => user.RoleIds.Contains(p.ParentRoleId));
            }
            return false;
        }

        public void PermitRole(ulong guildId, ulong childRoleId, ulong parentRoleId)
        {
            GuildConfig g;
            if (data.TryGetValue(guildId, out g))
            {
                var r = g.RolePermissions.SingleOrDefault(p => p.ParentRoleId == parentRoleId);
                if (r == null)
                {
                    r = new AllowedRole() { ParentRoleId = parentRoleId, ChildRoleIds = { childRoleId } };
                }
                if (!r.ChildRoleIds.Contains(childRoleId)) { r.ChildRoleIds.Add(childRoleId); }
            }
            else {
                g = new GuildConfig() { GuildId = guildId };
                g.RolePermissions.Add(new AllowedRole() { ParentRoleId = parentRoleId, ChildRoleIds = { childRoleId } });
                data[guildId] = g;
            }
        }

        public void DenyRole(ulong guildId, ulong childRoleId, ulong parentRoleId)
        {
            GuildConfig g;
            if (data.TryGetValue(guildId, out g))
            {
                var r = g.RolePermissions.SingleOrDefault(p => p.ParentRoleId == parentRoleId);
                if (r != null && r.ChildRoleIds.Contains(childRoleId))
                {
                    r.ChildRoleIds.Remove(childRoleId);
                }
            }
        }

        public ReadOnlyCollection<AllowedRole> ListRolePermissions(ulong guildId)
        {
            return data[guildId].RolePermissions.AsReadOnly();
        }
    }
}
