﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoleWithIt.Data
{
    public class TagPermission
    {
        public ulong GuildID { get; set; }
        public ulong RoleID { get; set; }
    }
}
