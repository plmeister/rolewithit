﻿FROM mcr.microsoft.com/dotnet/runtime:5.0 AS runtime
WORKDIR /app
COPY . .
ENTRYPOINT ["dotnet", "./RoleWithIt.dll"]
