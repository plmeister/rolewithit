﻿using Discord;
using Discord.Commands;
using Discord.WebSocket;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using RoleWithIt.Data;
using RoleWithIt.Services;
using System;
using System.Threading.Tasks;

namespace RoleWithIt
{
    class Program
    {
        static void Main(string[] args) => new Program().MainAsync().GetAwaiter().GetResult();

        IConfigurationRoot Configuration { get; set; }

        public async Task MainAsync()
        {
            using (var services = ConfigureServices())
            {
                var client = services.GetRequiredService<DiscordSocketClient>();
                client.Log += LogAsync;
                services.GetRequiredService<CommandService>().Log += LogAsync;
                var botoptions = new BotOptions();
                Configuration.GetSection("bot").Bind(botoptions);
                await client.LoginAsync(TokenType.Bot, botoptions.Token);
                client.Connected += () =>  {
                    Task.Factory.StartNew(async () => {
                        await LogAsync(new LogMessage(LogSeverity.Info, "Connected", $"Waiting for 10 seconds before downloading guild members"));
                        await Task.Delay(10000);
                        foreach (var g in client.Guilds)
                        {
                            await LogAsync(new LogMessage(LogSeverity.Info, "Connected", $"Downloading users for guild {g.Id}"));
                            await g.DownloadUsersAsync();
                            await LogAsync(new LogMessage(LogSeverity.Info, "Connected", $"Downloaded users for guild {g.Id}"));
                            await Task.Delay(5000);
                        }
                    });
                    return Task.CompletedTask;
                };
                await client.StartAsync();

                await services.GetRequiredService<CommandHandlingService>().InitialiseAsync();

                await Task.Delay(-1);
            }
        }


        private Task LogAsync(LogMessage log)
        {
            Console.WriteLine(log.ToString());
            return Task.CompletedTask;
        }

        private ServiceProvider ConfigureServices()
        {
            var devEnvironmentVariable = Environment.GetEnvironmentVariable("NETCORE_ENVIRONMENT");
            var isDevelopment = !string.IsNullOrEmpty(devEnvironmentVariable) && devEnvironmentVariable == "development";

            var builder = new ConfigurationBuilder();
            builder.AddJsonFile(System.IO.Path.Combine(Environment.GetEnvironmentVariable("DATA"), "auth.json"));

            if (isDevelopment)
            {
                builder.AddUserSecrets<Program>();
            }

            Configuration = builder.Build();

            return new ServiceCollection()
                //.Configure<BotOptions>(Configuration.GetSection("bot"))
                .AddOptions()
                .AddSingleton<IConfigurationRoot>(Configuration)
                .AddSingleton<DiscordSocketClient>()
                .AddSingleton<CommandService>()
                .AddSingleton<CommandHandlingService>()
                .AddTransient<IRoleData>(s => new DapperRoleData(s))
                .AddSingleton<RoleService>()
                .AddSingleton<TagService>()
                .BuildServiceProvider();
        }
    }
}
