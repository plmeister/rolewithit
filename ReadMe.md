To get this running on your local machine, you'll need to add a user secrets file to store the bot token for logging in to discord

login to the discord developer portal, and create a new app and add a bot user to it - that's where you get the token from

then go into the project root, and execute this command:

`dotnet user-secrets set bot:token "<bot token for discord>"`
