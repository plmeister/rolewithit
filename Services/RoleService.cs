﻿using Discord;
using Discord.Rest;
using Discord.WebSocket;
using RoleWithIt.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoleWithIt.Services
{
    public class RoleService
    {
        public IRoleData _roledata { get; set; }
        public RoleService(IRoleData roleData)
        {
            _roledata = roleData;
        }

        public async Task<IEnumerable<IGuildUser>> addUsersToRole(IGuildUser caller, SocketRole role, IEnumerable<IGuildUser> users)
        {
            if (!await _roledata.IsUserAllowedToModifyRole(caller, role.Id))
                throw new ApplicationException($"User {caller.Username} is not allowed to modify role {role.Name} in {role.Guild.Name}");

            foreach (var u in users)
            {
                await u.AddRoleAsync(role);
            }
            return users;
        }

        public async Task<IEnumerable<IGuildUser>> delUsersFromRole(IGuildUser caller, SocketRole role, IEnumerable<IGuildUser> users)
        {
            if (!await _roledata.IsUserAllowedToModifyRole(caller, role.Id))
                throw new ApplicationException($"User {caller.Username} is not allowed to modify role {role.Name} in {role.Guild.Name}");

            foreach (var u in users)
            {
                await u.RemoveRoleAsync(role);
            }
            return users;
        }

        public async Task<List<SocketGuildUser>> removeAllUsersFromRole(IGuildUser caller, SocketRole role)
        {
            if (!await _roledata.IsUserAllowedToModifyRole(caller, role.Id))
                throw new ApplicationException($"User {caller.Username} is not allowed to modify role {role.Name} in {role.Guild.Name}");

            var removedUsers = new List<SocketGuildUser>();
            var removeUsers = role.Members.ToList();
            for (var i = 0; i < removeUsers.Count; i++)
            {
                await removeUsers[i].RemoveRoleAsync(role);
                removedUsers.Add(removeUsers[i]);
            }
            return removedUsers;
        }

        public class EventRoleUsers
        {
            public List<SocketGuildUser> UsersAdded { get; set; } //= new List<SocketGuildUser>();
            public List<SocketGuildUser> UsersRemoved { get; set; } //= new List<SocketGuildUser>();
            public List<SocketGuildUser> RoleContains { get; set; }
            public string ErrorMessage { get; set; }
            public string EventText { get; set; }

        }

        public async Task<EventRoleUsers> setEventRole(IGuildUser caller, SocketRole role, ISocketMessageChannel channel)
        {
            if (!await _roledata.IsUserAllowedToModifyRole(caller, role.Id))
                throw new ApplicationException($"User {caller.Username} is not allowed to modify role {role.Name} in {role.Guild.Name}");

            var result = new EventRoleUsers();
            RestMessage pinned = null;
            try
            {
                pinned = (await channel.GetPinnedMessagesAsync()).SingleOrDefault(p => p.Author.IsBot && p.Author.Username == "YAGPDB.xyz" && p.Embeds.Count == 1);
            }
            catch (InvalidOperationException ex)
            {
                result.ErrorMessage = "Too many pinned event messages found in this channel - I only need one";
                return result;
            }

            if (pinned == null)
            {
                result.ErrorMessage = $"No pinned event message found in this channel";
                return result;
            }

            result.EventText = pinned.Embeds.First().Title;

            var usersToRemove = role.Members.ToList();
            var usersToAdd = new List<SocketGuildUser>();
            var roleContains = new List<SocketGuildUser>();

            foreach (var f in pinned.Embeds.First().Fields)
            {
                if (f.Name.StartsWith("Participants"))
                {
                    var names = f.Value.Split("\n");
                    foreach (var n in names)
                    {
                        var user = role.Guild.Users.SingleOrDefault(u => $"{u.Username}#{u.Discriminator}" == n);
                        if (user != null)
                        {
                            var x = usersToRemove.FirstOrDefault(u => u.Id == user.Id);
                            if (x != null)
                            {
                                // don't need to remove this user
                                usersToRemove.Remove(x);
                            }
                            else
                            {
                                // user will need to be added
                                usersToAdd.Add(user);
                            }
                            roleContains.Add(user);
                        }
                    }
                }
            }

            if (usersToRemove.Count > 0) await delUsersFromRole(caller, role, usersToRemove);
            if (usersToAdd.Count > 0) await addUsersToRole(caller, role, usersToAdd);

            result.UsersAdded = usersToAdd;
            result.UsersRemoved = usersToRemove;
            result.RoleContains = roleContains;
            return result;
        }

        public async Task permitRole(ulong guildId, ulong childRoleId, ulong parentRoleId)
        {
            await _roledata.PermitRole(guildId, childRoleId, parentRoleId);
        }

        public async Task denyRole(ulong guildId, ulong childRoleId, ulong parentRoleId)
        {
            await _roledata.DenyRole(guildId, childRoleId, parentRoleId);
        }
    }
}

