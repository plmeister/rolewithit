﻿using Discord;
using Discord.Rest;
using Discord.WebSocket;
using RoleWithIt.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace RoleWithIt.Services
{
    public class TagService
    {
        public IRoleData _roledata { get; set; }
        public TagService(IRoleData roleData)
        {
            _roledata = roleData;
        }

        Regex tagRegex = new Regex(@"^\[.+?\]\s*", RegexOptions.Compiled);
        public async Task<Dictionary<IGuildUser, string>> tagUsers(string tag, IEnumerable<IGuildUser> users)
        {
            var result = new Dictionary<IGuildUser, string>();
            foreach (var u in users)
            {
                var currentNickname = string.IsNullOrEmpty(u.Nickname) ? u.Username : u.Nickname;
                try
                {
                    string newNickname = $"[{tag}] {currentNickname}";
                    if (tagRegex.IsMatch(currentNickname))
                    {
                        newNickname = tagRegex.Replace(currentNickname, $"[{tag}] ");
                    }
                    await u.ModifyAsync(p => p.Nickname = newNickname);
                    result[u] = $"'{currentNickname}' => '{newNickname}'";
                }
                catch (Exception ex)
                {
                    result[u] = $"Unable to set nickname for '{currentNickname}' (possible reason: can't set a nickname for a user higher than myself in the role hierarchy";
                }
            }
            return result;
        }

        public async Task<Dictionary<IGuildUser, string>> untagUsers(IEnumerable<IGuildUser> users)
        {
            var result = new Dictionary<IGuildUser, string>();
            foreach (var u in users)
            {
                var currentNickname = string.IsNullOrEmpty(u.Nickname) ? u.Username : u.Nickname;
                try
                {
                    var newNickname = currentNickname;
                    if (tagRegex.IsMatch(currentNickname))
                    {
                        newNickname = tagRegex.Replace(currentNickname, "");
                    }
                    if (newNickname != currentNickname)
                        await u.ModifyAsync(p => p.Nickname = newNickname);

                    result[u] = $"'{currentNickname}' => '{newNickname}'";
                }
                catch (Exception ex)
                {
                    result[u] = $"Unable to set nickname for '{currentNickname}' (possible reason: can't set a nickname for a user higher than myself in the role hierarchy";
                }
            }
            return result;
        }

        public async Task PermitRole(ulong guildId, ulong roleId)
        {
            await _roledata.PermitTag(guildId, roleId);
        }

        public async Task DenyRole(ulong guildId, ulong roleId)
        {
            await _roledata.DenyTag(guildId, roleId);
        }

        public async Task<IEnumerable<TagPermission>> ListPermissions(ulong guildId)
        {
            var perms = await _roledata.ListTagPermissions(guildId);
            return perms;
        }
    }
}

