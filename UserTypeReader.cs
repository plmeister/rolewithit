﻿using Discord.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoleWithIt
{
    public class UserTypeReader : TypeReader
    {
        public static bool UserMatch(Discord.IGuildUser user, string match)
        {
            return
                user.Username.Equals(match, StringComparison.OrdinalIgnoreCase)
                || (user.Nickname?.Equals(match, StringComparison.OrdinalIgnoreCase) ?? false)
                || user.Username.Contains(match, StringComparison.OrdinalIgnoreCase)
                || (user.Nickname?.Contains(match, StringComparison.OrdinalIgnoreCase) ?? false);
        }
        public static bool UserLooseMatch(Discord.IGuildUser user, string match)
        {
            return user.Nickname?.Contains(match, StringComparison.OrdinalIgnoreCase) ?? false
                || user.Username.Contains(match, StringComparison.OrdinalIgnoreCase);
        }
        public override async Task<TypeReaderResult> ReadAsync(ICommandContext context, string input, IServiceProvider services)
        {
            var users = await context.Guild.GetUsersAsync(Discord.CacheMode.AllowDownload, new Discord.RequestOptions() { Timeout = 30000 });
            
            if (input.StartsWith("<@") && input.EndsWith(">"))
            {
                var id = input[2..^1];
                var user = await context.Guild.GetUserAsync(ulong.Parse(id), Discord.CacheMode.AllowDownload);
                if (user != null)
                {
                    return TypeReaderResult.FromSuccess(user);
                }
                else
                {
                    return TypeReaderResult.FromError(CommandError.ParseFailed, $"Unable to match user id '{input}'");
                }
            } else
            {
                var matches = users.Where(u => UserMatch(u, input));

                if (matches.Count() == 1)
                {
                    return TypeReaderResult.FromSuccess(matches.First());
                } 
                else if (matches.Count() > 1)
                {
                    return TypeReaderResult.FromError(CommandError.ParseFailed, $"Found {matches.Count()} possible username/nickname matches for '{input}'");
                }
            }
            return TypeReaderResult.FromError(CommandError.ParseFailed, $"Unable to find user '{input}'");
        }
    }
}
