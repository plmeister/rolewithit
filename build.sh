﻿#!/bin/sh

docker pull mcr.microsoft.com/dotnet/sdk:5.0
docker run -v $(pwd):/app -w /app mcr.microsoft.com/dotnet/sdk:5.0 dotnet publish -c release -o out
docker build -t plmeister/rolewithit out
